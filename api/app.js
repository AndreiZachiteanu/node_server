var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
var cors = require("cors");
var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
let onlineClients = new Set();
const { allowedNodeEnvironmentFlags } = require("process");
testAPIRouter = require("./routes/testAPI");
var atob = require("atob");
var btoa = require("btoa");
var MongoClient = require("mongodb").MongoClient;
const { Console } = require("console");
var connections = [];
var url = "mongodb://localhost:27017/";
var db = null;
MongoClient.connect(url, { useUnifiedTopology: true }, function (err, client) {
  if (err) throw err;
  db = client.db("WebAppDB");
});

var server = require("http").createServer(app);
var io = require("socket.io")(server, {
  cors: {
    origin: "*",
  },
});
server.listen(8080);
console.info("Started listening");
io.on("connection", function (socket) {
  socket.on("hello", function (msg) {
    console.log("message: " + msg);
  });
  connections.push({
    conSocket: socket,
    username: "",
    password: "",
    loggedIn: false,
  });
  socket.on("login", function (msg1) {
    console.info("login attempt");
    if (db == null) return;

    var username = msg1.split(",")[0];
    var password = msg1.split(",")[1];
    console.info("Client looking for " + username + " with pass: " + password);
    try {
      db.collection("Users").findOne(
        { username: username.toLowerCase(), password: password },
        function (findError, result) {
          if (findError) {
            console.info("gonna throw an error \n");
            console.info(findError);
          }
          var user = result;

          console.info(user);
          if (user != null) {
            SaveLogin(result, socket);
            console.info("sending credential because they are correct");
          } else {
            socket.emit("loggedIn", "Credentials are incorrect");
          }
        }
      );
    } catch (error) {
      console.info(error);
    }
  });
  /*
  socket.on("fetchDevices", db.collection("Devices").find({}).toArray(function(err, res){
    if(err) throw err;
    socket.emit("devList",res);
  }));

*/

  socket.on("fetchProjects", (msg) => {
    var fetchAuth = CheckIfAuthorized(socket);

    if (fetchAuth.authorized) {
      console.log("x");
      //= [];
      db.collection("Projects")
        .find({})
        .toArray(function (err, result) {
          if (err) throw err;
          Array.from(result).forEach((project) => {
            Array.from(project.floor_plan).forEach((floorPlan) => {
              floorPlan.map = floorPlan.map.buffer;
            });
          });

          socket.emit("projectList", result);
        });

      /*
      var projectList = [];
      for(var i = 0; i< projects.length; i++){
        for(var y = 0; y < projects[i].shared_with.length;y ++){
          if(fetchAuth.username === projects[i].shared_with[y]){
            projectList.push(projects[i]);
          }
        }
      }
    
    if(typeof projects !== "undefined"){
      console.log("projects");
    console.log(projects);
    console.log(projectList);
    
    
    }
    */
    }
  });

  socket.on("insertNewProject", function (data) {
    if (data.new_project) {
      //console.log(data.buffer);
      /*      app.get("/", function (req, res) {
        res.render("index", { title: "Something else", imageData:  null});// "data:image/jpeg;base64," + data.buffer });
      });
      */
      Array.from(data.floor_plan).forEach((item) => {
        item.map = new Buffer.from(item.map, "base64"); //.toString();//atob(item.map);
        //console.info(item.map);
      });

      if (CheckIfAuthorized(socket).authorized) {
        var projectDB = db.collection("Projects");
        projectDB.insertOne(data, function (err, res) {
          if (err) throw err;
          console.info("project added");
          //db.close();
        });
        socket.emit("imageReceived", data);
      }
    }
  });
  socket.on("regNewDev", (message) => {
    if (CheckIfAuthorized(socket).authorized) {
      if (message != null) {
        data = message.icon;
        message.icon = new Buffer.from(data, "base64");

        db.collection("Devices").insertOne(message);
      }
    }
  });

  socket.on("fetchDevices", (msg) => {
    console.info("devices asked for");
    if (CheckIfAuthorized(socket).authorized) {
      db.collection("Devices")
        .find({})
        .toArray(function (err, res) {
          if (err) throw err;
          Array.from(res).forEach((dev) => {
            dev.icon = dev.icon.buffer;
          });

          console.log("show result");
          console.log(res);
          socket.emit("deviceList", res);
        });
    }
  });
});
/*
function UserCheck(findError, result) {
  if (findError) {
    console.info("gonna throw an error \n");
    console.info(findError);
  }
  var user = result;
  console.info("password is " + password);
  if (password == user.password) {
    console.info("found password");
  }
}
*/

function CheckIfAuthorized(socket) {
  var con = connections.find(function (item) {
    if (item.conSocket.id == socket.id) {
      return item;
    }
    return null;
  });

  return { authorized: con.loggedIn, username: con.username };
}

function SaveLogin(result, socket) {
  var con = connections.find(function (item) {
    if (item.conSocket.id == socket.id) {
      return item;
    }
    return null;
  });

  //console.info("cons is "); console.info(con.conSocket.id); console.info(" - results is "); console.info(socket.id);
  con.username = result.username;
  con.password = result.password;
  con.loggedIn = true;

  console.info("user has logged in - " + con.username);
  socket.emit("loggedIn", "Credentials are correct");
}

function DeleteConnection(socket) {
  var user = "";
  for (var i = 0; i < connections.length; i++) {
    if (socket.id == connections[i].socket.id) {
      user = connections[i].username;
      connections = connections.splice(i, 1);
    }
  }
  console.info("user has logged out - " + user);
}
var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(logger("dev"));
app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/testAPI", testAPIRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});
app.get("/", function (req, res, next) {
  res.render("test", { title: "Some other HTTP", imageData: null });
});

module.exports = app;
