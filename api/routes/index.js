var express = require("express");
var router = express.Router();


router.get("/", function (req, res, next) {
  res.render("index", { title: "Test HTTP" , imageData: null});
});

module.exports = router;
